# Project to capture links and documents that contribute to AWS deployments
- [How to set up multi-account AWS SAM deployments with GitLab CI/CD](https://about.gitlab.com/blog/2019/02/04/multi-account-aws-sam-deployments-with-gitlab-ci/)
- [Using GitLab CI/CD Pipeline to Deploy AWS SAM Applications](https://aws.amazon.com/blogs/apn/using-gitlab-ci-cd-pipeline-to-deploy-aws-sam-applications/)
